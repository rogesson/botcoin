class Brain

  def initialize(percentage_to_buy: 5, percentage_to_sell: 5)
    @percentage_to_buy  = percentage_to_buy
    @percentage_to_sell = percentage_to_sell

    @bought_in = 0
    @sold_in   = 0

    @last_price = 0
  end

  def can_buy?(price)
    percentage = percent_of(sold_in, @percentage_to_buy)
    sold_in_with_percentage = sold_in - percentage

    price < sold_in_with_percentage
  end

  def can_sell?(price)
    percentage = percent_of(bought_in, @percentage_to_sell)
    bought_in_with_percentage = bought_in + percentage

    price > bought_in_with_percentage
  end

  #[TODO] create Utils class
  def percent_of(value, percentage)
    value * percentage / 100
  end

  private

    attr_accessor :bought_in, :sold_in, :last_price
end
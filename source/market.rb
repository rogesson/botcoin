require "uri"
require "net/http"
require "json"
require_relative "price_collector.rb"
require_relative "brain.rb"

class Market
  API_URL      = 'https://api.bitcointrade.com.br/v2/public/'
  DEBUG_MODE   = true
  FIVE_MINUTES = 300
  TAX          = 0.25
  TOKEN        = "TOKEN"

  attr_reader :wallet, :budget

  def initialize(budget: balance, price_collector:)
    @price_collector = price_collector
    @budget = budget
    @initial_budget = @budget
    @price_to_buy   = @budget
    @price_to_sell  = @budget

    @wallet = 0

    #@bought_in = 0
    #@sold_in   = 0

    #[TODO] Change to nil
    @last_action = :sell

    @percent_to_buy = 1
    @percent_to_sell = 1
    @percent_stop_loss = 0.4
    #@last_price = 0
    @index = 0

    @brain = Brain.new

    puts "------"
    puts "Percent to buy #{@percent_to_buy}"
    puts "Percent to sell #{@percent_to_sell}"
    puts "Initial budget: R$ #{@budget}"
    puts "------"
  end

  def execute
    puts "[DEBUG_MODE: #{DEBUG_MODE}] Running #{Time.now}"
    write_log("id,wait,buy,sell,price")

    while current_price.size != 0
      price = current_price.shift

      if @wallet == 0
        perform_buy(price)
        @index += 1
        next
      end

      if @last_action == :sell
        can_buy?(price) ? perform_buy(price) : write_skip(price)
      else
        can_sell?(price) ? perform_sell(price) : write_skip(price)
      end

      @index += 1
    end

    puts "------"
    puts "Budget: R$: #{@budget}"
    puts "Wallet: #{@wallet}"
    puts "Profit: R$: #{wallet_value - @initial_budget}"
    puts "Total:  R$: #{wallet_value}"
  end

  def collect
    index = 0
    loop do
      price = @price_collector.current_price

      write_log("#{index}#{Time.now},#{price.first}")
      index += 1
      sleep 120
    end
  end

  private

    def current_price
      @current_price ||= @price_collector.current_price
    end

    #move to the brain class
    def perform_buy(price)
      coin = to_btc(budget, price)
      @wallet      += coin - percent_of(coin, TAX)
      @budget      = 0
      @bought_in   = price
      @last_action = :buy
      @last_price = price
      write_log("#{@index},,#{price},,#{price}")
    end

    #move to the brain class
    def perform_sell(price)
      money = to_money(@wallet, price)
      @budget  += money - percent_of(money, TAX)
      @sold_in = price
      @wallet  = 0
      @last_action = :sell
      @last_price = price
      write_log("#{@index},,,#{price},#{price}")
    end

    def to_btc(money, price)
       (money / price.to_f).floor(8)
    end

    def to_money(btc, price)
      (btc * price.to_f).round
    end

    def percent_of(value, percentage)
      value * percentage / 100
    end

    def wallet_value
      [to_money(@wallet, @bought_in), @budget].max
    end

    def can_buy?(price)
      @brain.can_buy?(price)
    end

    def can_sell?(price)
      @brain.can_sell?(price)
    end

    def wallet_info
      "Wallet: #{@wallet} wallet_value: #{wallet_value} budget: #{@budget}"
    end

    def write_log(text)
      puts text
      #open("log_#{Time.now.strftime("%d-%m-%Y")}.csv", 'a+') do |f|
      open("trade_result.csv", 'a+') do |f|
        f.puts text
      end
    end

    def balance
      url = URI("https://api.bitcointrade.com.br/v2/wallets/balance")
      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = true

      request = Net::HTTP::Get.new(url)
      request["Content-Type"] = "application/json"
      request["Authorization"] = "ApiToken #{TOKEN}"
      response = https.request(request)
      response = JSON.parse response.read_body
      response["data"][0]["available_amount"]
    end

    def write_skip(price)
      write_log("#{@index},#{price},,,#{price}")
    end
end

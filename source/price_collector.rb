require 'net/http'
require 'json'

class PriceCollector
  COIN_DESK_URL     = "https://api.coindesk.com/v1/bpi/historical/close.json"
  BITCOIN_TRADE_URL = "https://api.bitcointrade.com.br/v2/public/"

  def initialize(start_date: nil, end_date: nil, historic: nil)
    @start_date = start_date
    @end_date   = end_date
    @historic   = historic
  end

  def current_price
    if @historic
      @prices = @historic
      return @prices
    end

		if @start_date.nil?
      @prices = [price]
    else
      @prices ||= price_from_date
    end
  end

  private

    def price_from_date
      price_uri = COIN_DESK_URL + "?start=#{@start_date}&end=#{@end_date}"
      puts "Sending request to #{price_uri}"

      #[TODO] Create Request Class

      url = URI(price_uri)

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = true

      request = Net::HTTP::Get.new(url)
      request["Content-Type"] = "application/json"

      response = https.request(request)
      response = JSON.parse(response.read_body)
      response["bpi"].values
    end

    def price
      url = URI(BITCOIN_TRADE_URL + "BRLBTC/ticker")

      https = Net::HTTP.new(url.host, url.port);
      https.use_ssl = true

      request = Net::HTTP::Get.new(url)
      request["Content-Type"] = "application/json"

      response = https.request(request)
      response = JSON.parse(response.read_body)
      response["data"]["buy"]
    rescue => e
      puts "Error: #{e} | Response #{response} | #{e.backtrace}"
      [:error]
    end
end


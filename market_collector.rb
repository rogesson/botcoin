require_relative 'source/market.rb'
require_relative 'source/price_collector'

price_collector = PriceCollector.new

market = Market.new(budget: 700, price_collector: price_collector)
market.collect

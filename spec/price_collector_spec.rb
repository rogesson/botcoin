require_relative "../source/price_collector.rb"

describe "PriceCollector" do
	describe '#current_price' do
		context 'when two dates (start_date and end_date) are given' do
			it 'returns the price between two dates' do
				start_date  = '2019-12-01'
				end_date		= '2019-12-02'

				price_collector = PriceCollector.new(
					start_date: start_date, end_date: end_date
				)

				allow(price_collector).to receive(:price_from_date)
					.and_return([7420.8417, 7320.8167])

				expect(price_collector.current_price)
         .to eq([7420.8417, 7320.8167])
			end
		end

		context 'when an array of prices is given' do
			it 'returns this prices' do
				historic = [
					7420.80,
					7320.2417,
					7620.117,
					7720.8317,
					7920.5417,
					7120.6417
				]

				price_collector = PriceCollector.new(
					historic: historic
				)

				expect(price_collector.current_price)
				.to eq(historic)
			end
		end

		context 'when date is not given (current price)' do
			it 'returns the current price' do
				price_collector = PriceCollector.new

				allow(price_collector).to receive(:price)
					.and_return(42150.02)

				expect(price_collector.current_price)
          .to eq([42150.02])
			end
		end
	end
end

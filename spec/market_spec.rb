require_relative "../source/market.rb"

describe "Market" do
  describe "#execute" do
    it "spends all the money on the first buy" do
      budget = 700
      price_collector = instance_double(PriceCollector)
      
      allow(price_collector).to receive(:current_price)
        .and_return([7420])

      market = Market.new(budget: budget,
                          price_collector: price_collector)
      market.execute

      expect(market.budget).to eq(0)
      expect(market.wallet).to eq(0.09410377095)
    end

    it "spends all the coin on the first sell" do
      budget = 700
      current_price = 7420
      price_collector = instance_double(PriceCollector)

      allow(price_collector).to receive(:current_price)
        .and_return([7420, 8420])
      market = Market.new(budget: budget,
                          price_collector: price_collector)
      market.execute
      expect(market.budget).to eq(790.02)
      expect(market.wallet).to eq(0)
    end
  end
end

require_relative "../source/brain.rb"

describe 'Brain' do
  describe '#can_buy?' do
    it 'returns true if the price is X percent less than the last sale' do
      #5 percent of 9000 is: 450
      #900 - 450 is: 8550

      sold_in           = 9000
      price             = 8549 # less than 8550

      brain = Brain.new(
        percentage_to_buy: 5
      )

      allow(brain).to receive(:sold_in)
        .and_return(sold_in)

      expect(brain.can_buy?(price))
        .to be true
    end

    it 'returns false if the price is X percent less than the last sale' do
      #5 percent of 9000 is: 450
      #900 - 450 is: 8550

      sold_in           = 9000
      price             = 8551 #more than 8550

      brain = Brain.new(
        percentage_to_buy: 5
      )

      allow(brain).to receive(:sold_in)
        .and_return(sold_in)

      expect(brain.can_buy?(price))
        .to eq(false)
    end
  end

  describe '#can_sell?' do
    it 'returns true if the price is X percent more than the last purchase' do
      #5 percent of 9000 is: 450
      #900 - 450 is: 8550

      bought_in = 9000
      price = 9451 # more than 9450

      brain = Brain.new(
        percentage_to_buy: 5,
        percentage_to_sell: 5
      )

      allow(brain).to receive(:bought_in)
        .and_return(bought_in)

      expect(brain.can_sell?(price))
        .to be true
    end

    it 'returns false if the price is X percent less than the last purchase' do
      #5 percent of 9000 is: 450
      #900 - 450 is: 8550

      bought_in = 9000
      price = 9449 # less than 9450

      brain = Brain.new(
        percentage_to_buy: 5,
        percentage_to_sell: 5
      )

      allow(brain).to receive(:bought_in)
        .and_return(bought_in)

      expect(brain.can_sell?(price))
        .to be false
    end
  end
end